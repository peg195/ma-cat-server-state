---
name: MA-CAT
address: 
group: 
version: 
team: AoMe,Cat
Account : true
---
# dfgg服务器

## 介绍

赶快加入我们吧！我在LZDCraft等候您的到来！
稳定的运行的服务:服务器常年不停机，给玩家最好的体验！
高质量玩家陪伴:玩家之间相处和睦！
丰富精彩的冒险世界:我们的服务器与时俱进！
我们的特色:
纯净的生存系统:我们服务器采用原生态纯净生存，无任何超辅助类插件及功能插件！
MAC & LINUX & WIN三端互通:我们支持MAC & Linux & WIN三端互通，为更多的玩家提供服务！
赶快通过下面的方式加入我们吧！我们在LZDCraft等您的到来！
官方玩家QQ交流群：708676445
意见投稿邮箱：1030231378@qq.com
服主唯一QQ：1030231378

## 服务器信息

|||
| :---: | :---: |
| 服务器地址 | mc.lzdcraft.top |
| 服务器版本 | 1.8.x-1.20.x |
| 服务器团队 | LZDCraft团队 |

## 链接

- [官网](https://www.lzdcraft.top)
